package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import sample.bundles.Bundles;
import sample.config.Config;
import sample.db.DataBaseManagement;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
//        Locale.setDefault(new Locale("ep"));
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource(Config.FXML_FILE));

        loader.setResources(Bundles.getBundle());

        Pane pane = loader.load();

        Scene scene = new Scene(pane, 600, 450);
        //scene.getStylesheets().add("sample/css/style.css");

        primaryStage.setOnCloseRequest(e -> {
            DataBaseManagement.GET_SINGLETON().closeConnection();
            System.exit(0);
        });

        primaryStage.setTitle(Bundles.getString("title.app"));
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
