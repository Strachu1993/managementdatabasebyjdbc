package sample.bundles;

import sample.config.Config;

import java.util.ResourceBundle;

public class Bundles {

    private Bundles(){}

    private static final ResourceBundle bundle = ResourceBundle.getBundle(Config.BUNDLES_SRC);

    public static final ResourceBundle getBundle() {
        return bundle;
    }

    public static final String getString(String key){
        return bundle.getString(key);
    }
}
