package sample.config;

public class Config {

    private Config(){}

    public static final String DATABASE_NAME = "baza";

    public static final String DRIVER = "org.apache.derby.jdbc.EmbeddedDriver"; // "org.apache.derby.jdbc.ClientDriver";

    public static final String JDBC_URL = "jdbc:derby:" + DATABASE_NAME + ";create=true;";

    public static final String FXML_FILE = "fxml/sample.fxml";

    public static final String BUNDLES_SRC = "sample.bundles.messages";

}

// SELECT * FROM city INNER JOIN person ON city.id_city = person.id_city
// SELECT p.id_person, p.name, c.name  FROM person p INNER JOIN city c ON p.id_city = c.id_city
