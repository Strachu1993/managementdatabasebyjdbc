package sample.config;

public class QuerySql {

    private QuerySql(){}

    public static final String CREATE_TABLE_PERMISSIONS =
            "Create table permissions (\n" +
             "\tid_permissions Int NOT NULL,\n" +
             "\tname Varchar(50) NOT NULL,\n" +
             " Primary Key (id_permissions))";

    public static final String CREATE_TABLE_CITY =
            "Create table city (\n" +
             "\tid_city Int NOT NULL,\n" +
             "\tname Varchar(50) NOT NULL,\n" +
             "\tzip_code Varchar(6) NOT NULL,\n" +
             "\tcountry Varchar(50) NOT NULL,\n" +
             " Primary Key (id_city))";

    public static final String CREATE_TABLE_PERSON =
            "Create table person (\n" +
            "\tid_person Int NOT NULL,\n" +
            "\tid_permissions Int NOT NULL,\n" +
            "\tid_city Int NOT NULL,\n" +
            "\tname Varchar(50) NOT NULL,\n" +
            "\tpassword Varchar(50) NOT NULL,\n" +
            " Primary Key (id_person))";

    public static final String INSERT_TO_TABLE(String tableName, int countParam){
        String query = "INSERT INTO "+ tableName +" VALUES (";

        for(int i=0 ; i<countParam ;i++){
            if(i == countParam-1)
                query += "?)";
            else
                query += "?, ";
        }

        return query;
    }

    public static final String SELECT(String tableName){
        return "SELECT * FROM " + tableName;
    }

    public static final String CLEAR_TABLE(String tableName){
        return "DELETE FROM " + tableName;
    }

    public static final String DELETE_TABLE(String tableName){
        return "DROP TABLE " + tableName;
    }

    public static final String ADD_FOREGIN_KEY(String foreginTable, String foreignKey, String primaryTable, String primaryKey){
        return "ALTER TABLE " + foreginTable + " ADD FOREIGN KEY (" + foreignKey + ") REFERENCES " + primaryTable + " (" + primaryKey + ") ON DELETE RESTRICT ON UPDATE RESTRICT";
    }

}
