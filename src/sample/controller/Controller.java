package sample.controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import sample.bundles.Bundles;
import sample.db.DataBaseManagement;
import sample.db.DefaultTables;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    private DataBaseManagement dataBaseManagement = null;
    private String selectedListElement = null;

    @FXML
    private ListView tables;

    @FXML
    private TextArea console;

    @FXML
    private TextField queryField;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        dataBaseManagement = DataBaseManagement.GET_SINGLETON();
    }

    @FXML
    public void handleMouseClickTables(){
        String element = (String) tables.getSelectionModel().getSelectedItem();
        if(null != element && !element.equals(selectedListElement)) {
            addMessageToConsole( Bundles.getString("select") + element, 1);
            selectedListElement = element;
        }
    }

    @FXML
    public void clickButtonShowTables(){
        refreshList();
    }

    @FXML
    public void clickButtonShowTablesStructure(){
        if(null != selectedListElement){
            addMessageToConsole(selectedListElement + Bundles.getString("structure"), 2);
            dataBaseManagement.getTableStructure(selectedListElement).forEach((t) -> addMessageToConsole(Bundles.getString("before.result") + t.toString(), 1));
        }
    }

    @FXML
    public void clickButtonShowData(){
        if(null != selectedListElement) {
            addMessageToConsole(selectedListElement + Bundles.getString("data"), 2);
            dataBaseManagement.getDataFromTable(selectedListElement).forEach((t) -> addMessageToConsole(Bundles.getString("before.result") + t, 1));
        }
    }

    @FXML
    public void clickButtonDeleteTable(){
        if(null != selectedListElement){
            try {
                dataBaseManagement.deleteTable(selectedListElement);
                addMessageToConsole(Bundles.getString("remove.table") + selectedListElement, 2);
                refreshList();
                selectedListElement = null;
            } catch (SQLException e) {
                addMessageToConsole(Bundles.getString("table.not.deleted") + selectedListElement, 2);
            }
        }
    }

    @FXML
    public void clickButtonLoadTables(){
        DefaultTables.setDefaultTables();
        refreshList();
    }

    @FXML
    public void clickButtonClearConsole(){
        console.clear();
    }

    @FXML
    public void clickButtonExecuteQuery(){
        if(null != queryField && !"".equals(queryField.getText()))
            addMessageToConsole(dataBaseManagement.executeSqlAndReturnResult(queryField.getText()), 2);
    }

    private void refreshList() {
        tables.getItems().clear();

        dataBaseManagement.getTablesName().forEach((t) -> {
            Platform.runLater(() -> {
                tables.getItems().addAll(t);
            });
        });
        selectedListElement = null;
    }

    private void addMessageToConsole(String text, int countBeforeLines) {
        String message = "";

        for(int i=0 ; i<countBeforeLines ; i++)
            message += "\n";

        message += text;

        console.appendText(message);
        console.setScrollTop(console.getLength());
    }

}
