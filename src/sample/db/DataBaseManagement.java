package sample.db;

import sample.bundles.Bundles;
import sample.config.Config;
import sample.config.QuerySql;
import sample.dto.TableStructure;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DataBaseManagement {

    private Connection connection = null;
    private static DataBaseManagement DATA_BASE_MANAGEMENT = null;

    private DataBaseManagement(){
        connectToDb();
    }

    public static DataBaseManagement GET_SINGLETON(){
        if(DATA_BASE_MANAGEMENT == null)
            DATA_BASE_MANAGEMENT = new DataBaseManagement();

        return DATA_BASE_MANAGEMENT;
    }

    private void connectToDb(){
        try {
            Class.forName(Config.DRIVER);
            connection = DriverManager.getConnection(Config.JDBC_URL);

            if(null != connection)
                System.out.println(Bundles.getString("connection.text") + "\n");

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void closeConnection(){
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(Bundles.getString("disconnect.text"));
    }

    public void executeSql(String sql){
        try {
            connection.createStatement().execute(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<String> getDataFromTable(String tableName){
        List<String> tableRows = null;
        try {
            List<TableStructure> rows = getTableStructure(tableName);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(QuerySql.SELECT(tableName));

            tableRows = new ArrayList<>();

            while(resultSet.next()){
                StringBuilder result = new StringBuilder("");
                for(int i=0 ; i<rows.size() ;i++)
                    result.append(resultSet.getObject(i + 1) + ", ");

                tableRows.add(result.substring(0, result.length() - 2));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tableRows;
    }

    public String executeSqlAndReturnResult(String sql){
        String result;

        try {
            if(sql.substring(0, 6).toUpperCase().equals("SELECT"))
                result = showResult(sql);
            else {
                connection.createStatement().execute(sql);
                return Bundles.getString("correctly.done");
            }
        } catch (SQLException e) {
            return Bundles.getString("sql.query.error") + sql;
        }
        return result;
    }


    private String showResult(String sql) throws SQLException {
        StringBuilder result = new StringBuilder("");

        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet resultSet = statement.executeQuery();
        ResultSetMetaData rsmd = resultSet.getMetaData();

        while(resultSet.next()){
            result.append(Bundles.getString("before.result"));
            for(int i=1 ; i<=rsmd.getColumnCount() ; i++)
                result.append(resultSet.getObject(i) + ", ");

            result = new StringBuilder(result.substring(0, result.length() - 2)).append("\n");
        }
        return result.toString();
    }

    public List<String> getTablesName(){
        List<String> tables = new ArrayList<>();

        try {
            DatabaseMetaData dbmd = connection.getMetaData();
            String[] types = {"TABLE"};
            ResultSet rs = dbmd.getTables(null, null, "%", types);

            while(rs.next())
                tables.add(rs.getString("TABLE_NAME"));

        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(tables.size() == 0)
            return Collections.emptyList();

        return tables;
    }

    public void deleteTable(String tableName) throws SQLException{
        connection.createStatement().execute(QuerySql.DELETE_TABLE(tableName));
    }

    public void deleteAlltables(){
        List<String> tables = getTablesName();
        boolean check = false;

        for(int i=0 ; i<tables.size() ; i++)
            try {
                deleteTable(tables.get(i));
            } catch (SQLException e) {
                check = true;
            }

        if(check)
            deleteAlltables();
    }

    public List<TableStructure> getTableStructure(String tableName) {
        List<TableStructure> tableRow = new ArrayList();

        try {
            PreparedStatement ps = connection.prepareStatement(QuerySql.SELECT(tableName));
            ResultSet rs = ps.executeQuery();
            ResultSetMetaData rsmd=rs.getMetaData();

            int count = rsmd.getColumnCount();

            for(int i=1 ; i<=count ; i++)
                tableRow.add(new TableStructure(rsmd.getColumnName(i), rsmd.getColumnTypeName(i)));

        } catch (SQLException e) {
            return Collections.emptyList();
        }

        return tableRow;
    }

    public boolean insertToTable(String tableName, Object...data){
        try {
            List<TableStructure> tableRows = getTableStructure(tableName);

            PreparedStatement preparedStatement = connection.prepareStatement(QuerySql.INSERT_TO_TABLE(tableName, tableRows.size()));

            for(int i=0 ; i<data.length ; i++)
                preparedStatement.setObject(i+1, data[i]);

            preparedStatement.execute();

        } catch (SQLException e) {
            return false;
        }
        return true;
    }

}
