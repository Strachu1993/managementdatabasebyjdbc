package sample.db;

import sample.config.QuerySql;

public class DefaultTables {

    private static DataBaseManagement db = DataBaseManagement.GET_SINGLETON();

    private DefaultTables(){}

    public static void setDefaultTables(){
        prepareTables();
        insertDataToTable();
    }

    private static void prepareTables(){
        db.deleteAlltables();

        db.executeSql(QuerySql.CREATE_TABLE_CITY);
        db.executeSql(QuerySql.CREATE_TABLE_PERMISSIONS);
        db.executeSql(QuerySql.CREATE_TABLE_PERSON);

        db.executeSql(QuerySql.ADD_FOREGIN_KEY("person", "id_permissions", "permissions", "id_permissions"));
        db.executeSql(QuerySql.ADD_FOREGIN_KEY("person", "id_city", "city", "id_city"));
    }

    private static void insertDataToTable(){

        int count = 0;
        db.insertToTable("permissions", ++count, "user");
        db.insertToTable("permissions", ++count, "none");
        db.insertToTable("permissions", ++count, "admin");

        count = 0;

        db.insertToTable("city", ++count, "Szprotawa", "67-300", "Polska");
        db.insertToTable("city", ++count, "Małomice", "67-320", "Polska");
        db.insertToTable("city", ++count, "Zielona Góra", "67-300", "Polska");
        db.insertToTable("city", ++count, "Warszawa", "67-300", "Polska");
        db.insertToTable("city", ++count, "Kraków", "67-300", "Polska");
        db.insertToTable("city", ++count, "Poznań", "67-300", "Polska");
        db.insertToTable("city", ++count, "Wrocław", "67-300", "Polska");

        count = 0;

        db.insertToTable("person", ++count, 1, 1, "Name", "pass");
        db.insertToTable("person", ++count, 1, 2, "Name2", "pass");
        db.insertToTable("person", ++count, 2, 3, "Name123", "pass");
        db.insertToTable("person", ++count, 2, 2, "Name123", "pass");
        db.insertToTable("person", ++count, 2, 5, "siemak", "pass");
        db.insertToTable("person", ++count, 2, 1, "eloelo", "pass");
        db.insertToTable("person", ++count, 2, 4, "Zdzislaw", "pass");
        db.insertToTable("person", ++count, 2, 6, "Genowefa", "pass");
    }
    
}
