package sample.dto;

public class TableStructure {

    private String name;
    private String type;

    public TableStructure(String name, String type){
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TableStructure tableRow = (TableStructure) o;

        if (name != null ? !name.equals(tableRow.name) : tableRow.name != null) return false;
        return type != null ? type.equals(tableRow.type) : tableRow.type == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TableStructure{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
